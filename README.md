# README #

### What is this repository for? ###

Terry Patterson's OCAS Take-Home Assignment 20191201

### How do I get set up? ###

* Summary of set up
1. Load the 'OCAS20191201_Backend.sln' Visual Studio Solution and start in Debug mode
2. Load the 'OCAS20191201_Frontend.sln' Visual Studio Solution and start in Debug mode

* Dependencies
This system is based on ASP.NET Core MVC and written for demonstration in Microsoft Visual Studio 2019 Community Edition.
Please note that the 'OCAS20191201_Backend' API service uses a self-signed certificate and will require a browser security override.

* Database configuration
The data is persisted in a Microsoft SQL Server LocalDB database file 'ocas20191201db.mdf' in the repository root.

### Who do I talk to? ###

* Terence Patterson
416-792-3626
patterson2001@hotmail.com

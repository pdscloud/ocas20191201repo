﻿using System.ComponentModel.DataAnnotations;

namespace ActivitySignup_Common.Models
{
    public class ActivitySignupItem
    {
        public int Id { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "First name is missing.")]
        public string FirstName { get; set; }
        [Display(Name = "Last name")]
        [Required(ErrorMessage = "Last name is missing.")]
        public string LastName { get; set; }
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Email address is missing."), EmailAddress(ErrorMessage = "Email address is not valid.")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please specify an activity.")]
        public string Activity { get; set; }
        public string Comments { get; set; }

    }
}

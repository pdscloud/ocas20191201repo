﻿using ActivitySignup_Common.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ActivitySignup_Frontend.Views.ActivitySignup
{
    public class IndexModel : PageModel
    {
        private static readonly HttpClient _httpClient = new HttpClient();
        private static readonly Uri _baseUri = new Uri("https://localhost:44345/api/");

        public IList<ActivitySignupItem> ActivitySignupItem { get;set; }

        public async Task OnGetAsync()
        {
            //ActivitySignupItem = await _context.ActivitySignupItems.ToListAsync();

            Task<string> getTask = _httpClient.GetStringAsync(
                new Uri(baseUri: _baseUri, relativeUri: "ActivitySignupItems")
            );

            getTask.Wait();

            ActivitySignupItem = JsonConvert.DeserializeObject<List<ActivitySignupItem>>(value: getTask.Result);

        }
    }
}

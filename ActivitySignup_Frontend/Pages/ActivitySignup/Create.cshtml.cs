﻿using ActivitySignup_Common.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ActivitySignup_Frontend.Views.ActivitySignup
{
    public class CreateModel : PageModel
    {
        private static readonly HttpClient _httpClient = new HttpClient();
        private static readonly Uri _baseUri = new Uri("https://localhost:44345/api/");

        public CreateModel()
        {
        }
        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public ActivitySignupItem ActivitySignupItem { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Task postTask = _httpClient.PostAsync(
                requestUri: new Uri(baseUri: _baseUri, relativeUri: "ActivitySignupItems"),
                content: new StringContent(
                    content: JsonConvert.SerializeObject(value: ActivitySignupItem),
                    encoding: Encoding.UTF8, "application/json"
                )
            );

            await postTask;

            return RedirectToPage("/ActivitySignup/Index");
        }
    }
}

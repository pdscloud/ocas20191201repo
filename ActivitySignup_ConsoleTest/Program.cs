﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WebAPIClient
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();

        private static async Task ProcessRepositories()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json")
            );
            client.DefaultRequestHeaders.Add(
                "User-Agent", ".NET Foundation Repository Reporter"
            );

            var stringTask = client.GetStringAsync("https://api.github.com/orgs/dotnet/repos");

            var msg = await stringTask;
            Console.Write(msg);
        }

        private static async Task AddTestActivitySignups()
        {
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            Task postTask = null;

            postTask = client.PostAsync(
                requestUri: "https://localhost:44345/api/ActivitySignupItems", 
                content: new StringContent(
                    content: @"{
                        ""firstName"": ""Homer"",
	                    ""lastName"" : ""Simpson"",
	                    ""emailAddress"" : ""hjs@blah.com"",
	                    ""activity"" : ""Donut eating"",
	                    ""comments"" : ""Mmmmmmm""
                    }", 
                    encoding: Encoding.UTF8, "application/json"
                )
            );

            await postTask;

            postTask = client.PostAsync(
                requestUri: "https://localhost:44345/api/ActivitySignupItems",
                content: new StringContent(
                    content: @"{
                        ""firstName"": ""Marge"",
	                    ""lastName"" : ""Simpson"",
	                    ""emailAddress"" : ""ms @blah.com"",
	                    ""activity"" : ""Tolerating Homer"",
	                    ""comments"" : ""Hmmmmmm""
                    }", 
                    encoding: Encoding.UTF8, "application/json"
                )
            );

            await postTask;

        }

        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            //ProcessRepositories().Wait();
            AddTestActivitySignups().Wait();

            Console.WriteLine("DONE");

        }
    }
}

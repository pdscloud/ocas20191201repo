﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ActivitySignup_Backend.Models;

namespace ActivitySignup_Backend.Views.ActivitySignup
{
    public class DetailsModel : PageModel
    {
        private readonly ActivitySignup_Backend.Models.ActivitySignupContext _context;

        public DetailsModel(ActivitySignup_Backend.Models.ActivitySignupContext context)
        {
            _context = context;
        }

        public ActivitySignupItem ActivitySignupItem { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ActivitySignupItem = await _context.ActivitySignupItems.FirstOrDefaultAsync(m => m.Id == id);

            if (ActivitySignupItem == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}

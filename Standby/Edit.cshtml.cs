﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ActivitySignup_Backend.Models;

namespace ActivitySignup_Backend.Views.ActivitySignup
{
    public class EditModel : PageModel
    {
        private readonly ActivitySignup_Backend.Models.ActivitySignupContext _context;

        public EditModel(ActivitySignup_Backend.Models.ActivitySignupContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ActivitySignupItem ActivitySignupItem { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ActivitySignupItem = await _context.ActivitySignupItems.FirstOrDefaultAsync(m => m.Id == id);

            if (ActivitySignupItem == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(ActivitySignupItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActivitySignupItemExists(ActivitySignupItem.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ActivitySignupItemExists(int id)
        {
            return _context.ActivitySignupItems.Any(e => e.Id == id);
        }
    }
}

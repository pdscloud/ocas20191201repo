﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ActivitySignup_Backend.Models;

namespace ActivitySignup_Backend.Views.ActivitySignup
{
    public class DeleteModel : PageModel
    {
        private readonly ActivitySignup_Backend.Models.ActivitySignupContext _context;

        public DeleteModel(ActivitySignup_Backend.Models.ActivitySignupContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ActivitySignupItem ActivitySignupItem { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ActivitySignupItem = await _context.ActivitySignupItems.FirstOrDefaultAsync(m => m.Id == id);

            if (ActivitySignupItem == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ActivitySignupItem = await _context.ActivitySignupItems.FindAsync(id);

            if (ActivitySignupItem != null)
            {
                _context.ActivitySignupItems.Remove(ActivitySignupItem);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}

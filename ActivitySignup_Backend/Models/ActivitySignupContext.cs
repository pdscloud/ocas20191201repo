﻿using ActivitySignup_Common.Models;
using Microsoft.EntityFrameworkCore;

namespace ActivitySignup_Backend.Models
{
    public class ActivitySignupContext : DbContext
    {
        public ActivitySignupContext(DbContextOptions<ActivitySignupContext> options)
            : base(options)
        {
        }

        public DbSet<ActivitySignupItem> ActivitySignupItems { get; set; }

    }
}

﻿using ActivitySignup_Backend.Models;
using ActivitySignup_Common.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ActivitySignup_Backend.Controllers
{
    [Route("api/ActivitySignupItems")]
    [ApiController]
    public class ActivitySignupItemsController : ControllerBase
    {
        private readonly ActivitySignupContext _context;

        public ActivitySignupItemsController(ActivitySignupContext context)
        {
            _context = context;
        }

        // GET: api/ActivitySignupItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActivitySignupItem>>> GetActivitySignupItems()
        {
            return await _context.ActivitySignupItems.ToListAsync();
        }

        // GET: api/ActivitySignupItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActivitySignupItem>> GetActivitySignupItem(int id)
        {
            var activitySignupItem = await _context.ActivitySignupItems.FindAsync(id);

            if (activitySignupItem == null)
            {
                return NotFound();
            }

            return activitySignupItem;
        }

        // PUT: api/ActivitySignupItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActivitySignupItem(int id, ActivitySignupItem activitySignupItem)
        {
            if (id != activitySignupItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(activitySignupItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActivitySignupItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ActivitySignupItems
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ActivitySignupItem>> PostActivitySignupItem(ActivitySignupItem activitySignupItem)
        {
            _context.ActivitySignupItems.Add(activitySignupItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetActivitySignupItem), new { id = activitySignupItem.Id }, activitySignupItem);
        }

        // DELETE: api/ActivitySignupItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActivitySignupItem>> DeleteActivitySignupItem(int id)
        {
            var activitySignupItem = await _context.ActivitySignupItems.FindAsync(id);
            if (activitySignupItem == null)
            {
                return NotFound();
            }

            _context.ActivitySignupItems.Remove(activitySignupItem);
            await _context.SaveChangesAsync();

            return activitySignupItem;
        }

        private bool ActivitySignupItemExists(int id)
        {
            return _context.ActivitySignupItems.Any(e => e.Id == id);
        }
    }
}

﻿using ActivitySignup_Backend.Controllers;
using ActivitySignup_Backend.Models;
using ActivitySignup_Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ActivitySignup_Backend.Controllers.Tests
{
    [TestClass()]
    public class ActivitySignupItemsControllerTests
    {
        private readonly ActivitySignupContext _context;

        public ActivitySignupItemsControllerTests(ActivitySignupContext context)
        {
            _context = context;
        }

        [TestMethod()]
        public void PostActivitySignupItemTest()
        {
            ActivitySignupItem activitySignupItem = new ActivitySignupItem()
            {
                Id = 101,
                FirstName = "Terry",
                LastName = "Patterson",
                EmailAddress = "tpatterson@bogus.com",
                Activity = "Writing code",
                Comments = "All in green"
            };

            ActivitySignupContext activitySignupContext = new ActivitySignupContext(new Microsoft.EntityFrameworkCore.DbContextOptions<ActivitySignupContext>());
            ActivitySignupItemsController activitySignupItemsController = new ActivitySignupItemsController(context: activitySignupContext);

            var task = activitySignupItemsController.PostActivitySignupItem(activitySignupItem);
            task.Wait();

            Assert.Fail();
        }

        [TestMethod()]
        public void ActivitySignupItemsControllerTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetActivitySignupItemsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetActivitySignupItemTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void PutActivitySignupItemTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void PostActivitySignupItemTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void DeleteActivitySignupItemTest()
        {
            Assert.Fail();
        }
    }
}